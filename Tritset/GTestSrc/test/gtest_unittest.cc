#include "gtest/gtest.h"
#include "TritSet.h"

namespace {

	TEST(TritLogicTest, LogicNOT)
	{
		EXPECT_EQ(!False, True);
		EXPECT_EQ(!Unknown, Unknown);
		EXPECT_EQ(!True, False);
	}

	TEST(TritLogicTest, LogicAND)
	{
		EXPECT_EQ(False, False&False);
		EXPECT_EQ(False, False&Unknown);
		EXPECT_EQ(False, False&True);
		EXPECT_EQ(False, Unknown&False);
		EXPECT_EQ(Unknown, Unknown&Unknown);
		EXPECT_EQ(Unknown, Unknown&True);
		EXPECT_EQ(False, True&False);
		EXPECT_EQ(Unknown, True&Unknown);
		EXPECT_EQ(True, True&True);
	}

	TEST(TritLogicTest, LogicOR)
	{
		EXPECT_EQ(False, False | False);
		EXPECT_EQ(Unknown, False | Unknown);
		EXPECT_EQ(True, False | True);
		EXPECT_EQ(Unknown, Unknown | False);
		EXPECT_EQ(Unknown, Unknown | Unknown);
		EXPECT_EQ(True, Unknown | True);
		EXPECT_EQ(True, True | False);
		EXPECT_EQ(True, True | Unknown);
		EXPECT_EQ(True, True | True);
	}

	TEST(ReferenceTest, Test1)
	{
		TritSet setA(100);
		TritSet setB(100);

		const Trit T1 = setA[25];

		setA[25] = True;
		setB[25] = setA[25];

		EXPECT_EQ(Unknown, T1);
		EXPECT_EQ(True, setA[25]);
		EXPECT_EQ(True, setB[25]);
	}

	TEST(ReferenceTest, Test2)
	{
		TritSet setA(100);
		TritSet setB(100);

		const Trit T1 = setA[125];

		setA[125] = True;
		setB[125] = setA[125];

		EXPECT_EQ(Unknown, T1);
		EXPECT_EQ(True, setA[125]);
		EXPECT_EQ(True, setB[125]);
	}

	TEST(ReferenceTest, ConstReference)
	{
		TritSet setA(100);


		setA[125] = True;
		const TritSet setB(setA);
		const Trit T1 = setB[125];
		Trit T2 = setB[125];

		EXPECT_EQ(True, T1);
		EXPECT_EQ(True, T2);
		EXPECT_EQ(True, setA[125]);
		EXPECT_EQ(True, setB[125]);
	}

	TEST(IterTest, Test1)
	{
		TritSet setA(64);
		for (TritSet::iterator iter = setA.begin(); iter != setA.end(); ++iter)
			*iter = False;
		EXPECT_EQ(64, setA.cardinality(False));


	}
}

TEST(IterTest, Test2)
{
	TritSet setA(64);
	for (TritSet::iterator iter = setA.begin(); iter != setA.end(); ++iter)
		*iter = False;

	const TritSet setB(setA);
	for (TritSet::iterator iter = setB.begin(); iter != setB.end(); ++iter)
		EXPECT_EQ(*iter, False);

}

