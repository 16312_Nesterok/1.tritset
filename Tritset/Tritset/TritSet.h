#pragma once
#include <unordered_map>

enum Trit{False = 1, Unknown = 0, True = 2};

Trit operator!(const Trit);
Trit operator&(const Trit,const Trit);
Trit operator|(const Trit,const Trit);

class TritSet {
	static const char BITS_IN_BYTE = 8;
	static const char BITS_IN_TRIT = 2;
	static const char TRITS_IN_INT = sizeof(unsigned int) * BITS_IN_BYTE / BITS_IN_TRIT;
	
	size_t last;													// number of last defined trit
	size_t size;													// size of elem in ints !!!

	unsigned int* elem;
public:
	
	class reference {												// reference to a single trit
		friend class TritSet;
		TritSet& T;													// Reference to tritset
		size_t num;													// numer of trit represented
		reference(TritSet&, const size_t);							// for set[i]
		unsigned int writeMask(const size_t, const Trit) const;		// mask for writing value val into Nth trit 
	public:
		operator Trit() const;

		reference& operator=(const Trit x);							// for set[i] = x
		reference& operator=( reference& x);

	};
	class iterator {	
		friend class TritSet;
		reference ref;
		iterator(const reference&);									// iterator for tritset
	public:

		reference operator*() const;								// *p
		
		iterator& operator++();										// ++p
		iterator operator++(int);									// p++

		iterator& operator--();										// --p
		iterator operator--(int);									// p--

		bool operator==(const iterator&) const;						// p1 == p2
		bool operator!=(const iterator&) const;						// p1 != p2
	};

	class const_iterator {
		friend class TritSet;
		const TritSet& T;
		size_t n;
		const_iterator(const reference&);									// iterator for tritset
	public:

		reference operator*() const;								// *p

		iterator& operator++();										// ++p
		iterator operator++(int);									// p++

		iterator& operator--();										// --p
		iterator operator--(int);									// p--

		bool operator==(const iterator&) const;						// p1 == p2
		bool operator!=(const iterator&) const;						// p1 != p2
	};

	TritSet(size_t);												// constructor
	TritSet();														// default constructor
	TritSet(const TritSet&);
	~TritSet();														// destructor
	

	size_t capacity() const;										// max number of trit we can store without memory allocation
	size_t length() const;											// last known elem + 1
	void shrink();													// shrink to max known value
	size_t cardinality(const Trit) const;										// number of trits with one of values
	std::unordered_map< Trit, int, std::hash<int> > cardinality();	// same as previous, but for all values
	void trim(size_t);												// forgot all values from lastIndex												// logical legth � max known trit+1


	reference operator[](const size_t);								// for set[i]
	Trit operator[](const size_t) const;

	TritSet operator&(TritSet&);									// for TritSet setC = setA & setB;
	TritSet operator|(TritSet&);									// for TritSet setC = setA | setB;
	TritSet operator!();											// for TritSet setC = !setA;

	iterator begin();
	iterator end();

	class out_of_range {};											// out of range exception
	class iter_error {};											// iterator error exception
	
};

std::ostream& operator<<(std::ostream&, const TritSet&);