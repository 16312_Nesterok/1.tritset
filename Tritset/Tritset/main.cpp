#pragma once
#include "TritSet.h"
#include <iostream>
#include <new>
#include <string>

int main()
{
	try {
		TritSet setA(100);
		TritSet::iterator i = setA.begin();
		for (i; i != setA.end(); i++)
			*i = True;

		std::unordered_map< Trit, int, std::hash<int> > stat = setA.cardinality();
		std::cout << setA << std::endl;
	}
	catch (const std::bad_alloc& e) {									// catching bad_alloc exception
	std::cout << "Allocation failed: " << e.what() << std::endl;
	}
	catch (TritSet::out_of_range)
	{
	std::cout << "Tritset size is out of range" << std::endl;
	}
	catch (TritSet::iter_error)
	{
		std::cout << "Iterator is out of range" << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown Error" << std::endl;
	}
	return 0;
}