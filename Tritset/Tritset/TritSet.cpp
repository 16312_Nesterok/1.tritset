#pragma once
#include "TritSet.h"
#include <string>
#include <iostream>

Trit operator!(const Trit A)
{
	switch (A)
	{
	case False:
		return True;
	case Unknown:
		return A;
	case True:
		return False;
	}
	return A;
}

Trit operator&(const Trit A, const Trit B)
{
	if ((A == False) || (B == False))
		return False;
	else if ((A == Unknown) || (B == Unknown))
		return Unknown;
	else return True;
}

Trit operator|(const Trit A, const Trit B)
{
	if ((A == True) || (B == True))
		return True;
	else if ((A == Unknown) || (B == Unknown))
		return Unknown;
	else return False;
}

TritSet::TritSet(size_t last = 0)
	: size{ (last - 1) / (TRITS_IN_INT)+1 }, elem{ (last != 0) ? new unsigned int[size] : nullptr }, last{ 0 }
{
	if (last < 0)
		throw out_of_range();
	for (int i = 0; i< size; i++)
		elem[i] = 0;
}

TritSet::TritSet()
	: size{ 0 }, elem{ nullptr }, last{ 0 } {}

TritSet::TritSet(const TritSet& T)
{
	size = T.size;
	last = T.last;
	elem = new unsigned int[size];
	for (size_t i = 0; i < size; i++)
		elem[i] = T.elem[i];
}

TritSet::~TritSet()
{
	if (elem != nullptr)
		delete[] elem;
	elem = nullptr;
}

size_t TritSet::capacity() const
{
	return size*TRITS_IN_INT;
}

size_t TritSet::length() const
{
	return last+1;
}

TritSet::reference::operator Trit() const
{
	if (num + 1 > T.last)
		return Unknown;

	char mod = num % TRITS_IN_INT;
	size_t div = num / TRITS_IN_INT;
	unsigned int elem = T.elem[div];
	return static_cast<Trit>((elem << 2*mod) >>  (TRITS_IN_INT*BITS_IN_TRIT - 2));

}


TritSet::reference TritSet::operator[](const size_t n)
{
	reference ref(*this, n);
	return ref;
}

Trit TritSet::operator[](const size_t num) const
{
	if (num + 1 > last)
		return Unknown;

	char mod = num % TRITS_IN_INT;
	size_t div = num / TRITS_IN_INT;
	unsigned int eleml = elem[div];
	return static_cast<Trit>((eleml << 2 * mod) >> (TRITS_IN_INT*BITS_IN_TRIT - 2));
}



TritSet::reference::reference(TritSet& set, const size_t n)
	: T{ set }, num{ n } {}

unsigned int TritSet::reference::writeMask(const size_t n, const Trit T) const
{ 
	char mod = n % TRITS_IN_INT;
	unsigned int i = static_cast<unsigned int>(T);
	return (i << (TRITS_IN_INT*BITS_IN_TRIT-2*mod-2));

}

TritSet::reference& TritSet::reference::operator=(const Trit x)
{
	if ((num+1) > T.capacity())
		if (x == Unknown)
			return *this;
		else 
		{
			
			size_t newsize = num / (TRITS_IN_INT) + 1;
			
			unsigned int* nelem = new unsigned int[newsize];

			for (int i = 0; i < newsize; i++)
				nelem[i] = 0;
			for (int i = 0; i < T.size; i++)
				nelem[i] = T.elem[i];

			T.elem = nelem;
			T.size = newsize;

			*this = x;
		}
	else {

		size_t div = num / TRITS_IN_INT;
		size_t mod = num % TRITS_IN_INT;
		T.elem[div] &= ~writeMask(mod, static_cast<Trit>(3));
		T.elem[div] |= writeMask(mod, x);

		if (num + 1 > T.last && (x != Unknown))
			T.last = num + 1;

	}
	return *this;
}

TritSet::iterator::iterator(const reference& r)
	: ref(r) 
{
	if (r.num < 0 || r.num > r.T.capacity())
		throw iter_error();
}

TritSet::reference TritSet::iterator::operator*() const
{
	return ref;
}

TritSet::reference& TritSet::reference::operator=( reference& x)
{
	T = x.T;
	num = x.num;

	return *this;
}

TritSet::iterator& TritSet::iterator::operator++()
{
	++(ref.num);
	ref = ref.T[ref.num];
	
	return *this;
}

TritSet::iterator TritSet::iterator::operator++(int)
{
	iterator buf = *this;
	++(ref.num);
	ref = ref.T[ref.num];
	
	return buf;
}

TritSet::iterator& TritSet::iterator::operator--()
{
	--(ref.num);
	ref = ref.T[ref.num];
	return *this;
}

TritSet::iterator TritSet::iterator::operator--(int)
{
	iterator buf = *this;
	--(ref.num);
	ref = ref.T[ref.num - 1];
	return buf;
}

bool TritSet::iterator::operator==(const iterator& iter2) const
{
	return (ref.num == iter2.ref.num);
}

bool TritSet::iterator::operator!=(const iterator& iter2) const
{
	return (ref.num != iter2.ref.num);
}






void TritSet::shrink()
{
	size_t newsize = (last - 1) / (TRITS_IN_INT)+1 ;
	unsigned int* nelem = new unsigned int[newsize];
	for (int i = 0; i < newsize; i++)
		nelem[i] = elem[i];
	delete[] elem;
	elem = nelem;
	size = newsize;
}

size_t TritSet::cardinality(const Trit value) const
{
	size_t num = 0;
	for (int i = 0; i < last; i++)
		if ((*this)[i] == value)
			num++;
	return num;
}

std::unordered_map< Trit, int, std::hash<int> > TritSet::cardinality()
{
	std::unordered_map< Trit, int, std::hash<int> > map;
	for (int i = 0; i < last; i++)
		switch ((*this)[i]) {
		case True:
			map[True]++;
			break;
		case Unknown:
			map[Unknown]++;
			break;
		case False:
			map[False]++;
			break;
		}
	return map;
}

void TritSet::trim(size_t lastIndex)
{
	size_t lim = lastIndex/TRITS_IN_INT + 1;
	for (size_t i = lastIndex; i < lim*TRITS_IN_INT; i++)
		(*this)[i] = Unknown;
	for (size_t i = lim; i < size; i++)
		elem[i] = 0;
	last = lastIndex;
}

TritSet TritSet::operator&(TritSet& setB)
{
	size_t size = ((capacity() > setB.capacity()) ? capacity() : setB.capacity());

	TritSet setC(size);
	for (int i = 0; i < size; i++)
		setC[i] = ((*this)[i] & setB[i]);
	return setC;
}

TritSet TritSet::operator|(TritSet& setB)
{
	size_t size = ((capacity() > setB.capacity()) ? capacity() : setB.capacity());

	TritSet setC(size);
	for (int i = 0; i <= size; i++)
		setC[i] = ((*this)[i] | setB[i]);
	return setC;
}

TritSet TritSet::operator!()
{
	TritSet setC(size);
	for (int i = 0; i < size; i++)
		setC[i] = !((*this)[i]);
	return setC;
}

TritSet::iterator TritSet::begin()
{
	iterator iter((*this)[0]);
	return iter;
}
TritSet::iterator TritSet::end()
{
	iterator iter((*this)[capacity()]);
	return iter;
}


std::ostream& operator<<(std::ostream& os, const TritSet& T)
{
	for (size_t i = 0; i < T.capacity(); i++)
	{
	

		if ((i % (sizeof(unsigned int) * 4) == 0) && (i != 0))
			os << std::endl;


		switch (T[i]) {
		case True:
			os << 'T';
			break;
		case Unknown:
			os << 'U';
			break;
		case False:
			os << 'F';
			break;
		}
	}
	return os;
}